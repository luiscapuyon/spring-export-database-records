package com.example.springexportdatabaserecords

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringExportDatabaseRecordsApplication

fun main(args: Array<String>) {
	runApplication<SpringExportDatabaseRecordsApplication>(*args)
}
